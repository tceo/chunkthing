#include "loader.h"

char *read_file(char *path, long *out_size) {
	FILE *file;
	long size;
	char *buffer;

	file = fopen(path, "rb");
	if (!file) return NULL;
	fseek(file, 0, SEEK_END);
	size = ftell(file);
	rewind(file);

	if (out_size) *out_size = size;

	buffer = calloc(1, size+1);
	fread(buffer, size, 1, file);
	fclose(file);

	return buffer;
}

uint8_t *load_webp_as_rgba(char *path, int *width, int *height) {
	long size;
	uint8_t *rgba;
	char *buffer = read_file(path, &size);
	WebPGetInfo(buffer, size, width, height);
	rgba = WebPDecodeRGBA(buffer, size, width, height);
	free(buffer);
	return rgba;
}
