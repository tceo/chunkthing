#include "window.h"

GLFWwindow *window;

bool init_window() {
	if (!glfwInit()) return false;
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, OGL_VERSION_MAJOR);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, OGL_VERSION_MINOR);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, true);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, false);

	window = glfwCreateWindow(200, 200, NAME, NULL, NULL);
	if (!window) return false;
	glfwMakeContextCurrent(window);
	return true;
}

GLFWwindow *get_window() {
	return window;
}

bool resize_window(int x, int y) {
	// TODO: write.
	return false;
}
