#include "defines.h"
#include "entity.h"

struct chunk {
	uint16_t tiles[TILE_LAYER_COUNT][CHUNK_SIZE][CHUNK_SIZE];
	struct entity_list entities[ENTITY_LAYER_COUNT];
};
