#pragma once

#include <GL/glew.h>
#include "chunk.h"
#include "loader.h"
#include <stdbool.h>

void init_render();

void update_tile_texture(uint8_t *rgba, uint16_t id);

GLuint compile_shader_from_file(char *path, GLenum type);

GLuint create_shader_program(char *frag_file, char *vert_file);
