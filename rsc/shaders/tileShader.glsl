#version 440

in vec3 vertColour;
out vec4 fragColour;

uniform usampler2D tilemap;

uniform sampler2D texture_atlas;

void main() {
     fragColour = texture(texture_atlas, vec2(0, 0)).rgba;
}